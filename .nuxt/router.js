import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _29b6c96e = () => import('..\\pages\\about.vue' /* webpackChunkName: "pages_about" */).then(m => m.default || m)
const _31c7a95c = () => import('..\\pages\\test\\question3.vue' /* webpackChunkName: "pages_test_question3" */).then(m => m.default || m)
const _31ab7a5a = () => import('..\\pages\\test\\question1.vue' /* webpackChunkName: "pages_test_question1" */).then(m => m.default || m)
const _31b991db = () => import('..\\pages\\test\\question2.vue' /* webpackChunkName: "pages_test_question2" */).then(m => m.default || m)
const _496ea433 = () => import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */).then(m => m.default || m)



if (process.client) {
  window.history.scrollRestoration = 'manual'
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected
  if (to.matched.length < 2) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise(resolve => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/about",
			component: _29b6c96e,
			name: "about"
		},
		{
			path: "/test/question3",
			component: _31c7a95c,
			name: "test-question3"
		},
		{
			path: "/test/question1",
			component: _31ab7a5a,
			name: "test-question1"
		},
		{
			path: "/test/question2",
			component: _31b991db,
			name: "test-question2"
		},
		{
			path: "/",
			component: _496ea433,
			name: "index"
		}
    ],
    
    
    fallback: false
  })
}
